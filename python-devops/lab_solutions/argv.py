#!/usr/bin/env python3
import sys
print('Program arguments', sys.argv)
print('Module name', __name__)


if __name__ == '__main__':
    print('Running as a script!')
else:
    print('Imported!')
