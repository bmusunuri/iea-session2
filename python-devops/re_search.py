#!/usr/bin/env python3
import re
linenum = 0

for line in open('poem.txt'):
    linenum += 1
    if re.search(r'\AI', line):
        print('{}: {}'.format(linenum, line), end='')
