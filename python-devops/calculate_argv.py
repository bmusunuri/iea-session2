#!/usr/bin/env python3
import math
import sys


def calculate_args(num1, num2, operator):
    print('Program arguments', sys.argv)

def calculate(num1, num2, operator):
    
 
    if operator == '+':
        result = num1 + num2
    elif operator == '-':
        result = num1 - num2
    elif operator== '*':
        result = num1 * num2
    elif operator== '/':
        try:
            result =num1/num2
        except ZeroDivisionError:
            print('ZeroDivisionError, cannot divide by 0')
            result = 0
    elif operator == 'log':
        try:
            result =math.log(num1)/math.log(num2)
        except (ZeroDivisonError, ValueError, TypeError):
            print(' please check the values')
            result=0
        else:
            print('everything ok')
        finally:
            print('execute')
        
    return result

if __name__ == '__main__':
    print('Runnin as a script')

    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')

    if len(sys.argv) !=4:
        print('Usage: calculate_gov OPERAND OPERAND OPERATOR')
        sys.exit(-1)

    try:    
        num1= float(sys.argv[1])
        num2= float(sys.argv[2])
        operator=sys.argv[3]
    except ValueError:
        print('Invalid Parameter! please supply numeric values')
    except IndexError:
        print('Usage: calculate_gov OPERAND OPERAND OPERATOR')
    else:
        print('Result:', calculate(num1, num2, operator))
else:
    print('Imported')
