#!/usr/bin/env python3

import sys
#import calculate( then we have to use calculate.calculate
from calculate_module import calculate

def calculate_args(num1, num2, operator):
    print('Program arguments', sys.argv)


if __name__ == '__main__':
    print('Runnin as a script')

    for idx, arg in enumerate(sys.argv):
        print(f'arg {idx} is {arg}')

    if len(sys.argv) !=4:
        print('Usage: calculate_gov OPERAND OPERAND OPERATOR')
        sys.exit(-1)

    try:
        num1= float(sys.argv[1])
        num2= float(sys.argv[2])
        operator=sys.argv[3]
    except ValueError:
        print('Invalid Parameter! please supply numeric values')
    except IndexError:
        print('Usage: calculate_gov OPERAND OPERAND OPERATOR')
    else:
        print('Result:', calculate(num1, num2, operator))
else:
    print('Imported')

