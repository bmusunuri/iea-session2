#!/usr/bin/env python3
word_list = []

while True:
    is_adding = True
    wordstring = input('Please enter words:').strip()
    if wordstring == 'quit':
        break
    if wordstring == '-':
        word_list.reverse()
    else:
        words = wordstring.split()
        for word in words:
            if word.startswith('-'):
                is_adding = False
                word = word.strip('-')
            if is_adding:
                word_list.append(word)
            else:
                if word in word_list:
                    word_list.remove(word)
