import math


class calculator():
    def __init__(self):
        self.total =0
        
    def display(self):
        print('the total = :', self.total)
        
    def clear(self):
        self.total=0

    def eval(self, num1, num2, operator):
        if operator == '+':
            self.add(num1, num2)    
        elif operator == '-':
            self.subtract(num1, num2)
        elif operator == '*':  
            self.multiply(num1, num2)
        elif operator == '/':
            self.division(num1, num2)
        else:
            print('something wrong')
              
    def add(self, num1, num2= 0):
        if num2 == 0:
           result = float(num1)+self.total
        else:
            result= float(num1)+ float(num2)
        self.total =result
        return self.total
    
    def subtract(self, num1, num2=0):
        if num2 == 0:
           result = float(num1)-self.total
        else:
            float(num1)-float(num2)        
        self.total =result
        return self.total
    
    def multiply(self, num1, num2=0):
        if num2 == 0:
           result = float(num1) * self.total
        else:
            result=float(num1) * float(num2)       
        self.total =result
        return self.total
    
    
    def divison(self, num1, num2=0):
             
        try:
            if num2 == 0:
                result =float(num1)/ self.total
            else:
                result =float(num1)/float(num2)
            self.total =result
                
        except ZeroDivisionError:
            print('ZeroDivisionError, cannot divide by 0')
            result = 0
        
        return self.total

