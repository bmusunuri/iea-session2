#!/usr/bin/env python3
import math
def calculate(num1, num2, operator):


    if operator == '+':
        result = num1 + num2
    elif operator == '-':
        result = num1 - num2
    elif operator== '*':
        result = num1 * num2
    elif operator== '/':
        try:
            result =num1/num2
        except ZeroDivisionError:
            print('ZeroDivisionError, cannot divide by 0')
            result = 0
    elif operator == 'log':
        try:
            result =math.log(num1)/math.log(num2)
        except (ZeroDivisonError, ValueError, TypeError):
            print(' please check the values')
            result=0
        else:
            print('everything ok')
        finally:
            print('execute')

    return result


